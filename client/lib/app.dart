import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter/material.dart';
import 'package:maggiolinapp/layout/main.dart';
import 'package:maggiolinapp/screens/introduction.dart';
import 'package:maggiolinapp/screens/routes/about_us.dart';
import 'package:maggiolinapp/screens/routes/chat.dart';
import 'package:maggiolinapp/screens/routes/game.dart';
import 'package:maggiolinapp/screens/routes/new_game.dart';
import 'package:maggiolinapp/screens/routes/profile.dart';
import 'package:maggiolinapp/screens/routes/settings.dart';
import 'package:maggiolinapp/screens/routes/term_of_service.dart';
import 'package:maggiolinapp/screens/sign_in_page.dart';

class App extends StatelessWidget {
  static const String introductionRoute = "/introduction";
  static const String aboutUs = "/aboutUs";
  static const String settingsRoute = "/settings";
  static const String chatRoute = "/chat";
  static const String gameDetailRoute = "/gameDetail";
  static const String profileRoute = "/profile";
  static const String termOfServiceRoute = "/termOfService";
  static const String newGameRoute = "/newGame";

  static props(BuildContext context) {
    return ModalRoute.of(context)?.settings.arguments;
  }

  const App({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
        title: 'Flutter Demo',
        theme: ThemeData(
            // primarySwatch: Colors.blue,
            // visualDensity: VisualDensity.adaptivePlatformDensity,
            ),
        home: StreamBuilder<User?>(
          stream: FirebaseAuth.instance.idTokenChanges(),
          builder: (context, snapshot) {
            if (snapshot.connectionState == ConnectionState.waiting) {
              return const Center(
                child: CircularProgressIndicator(),
              );
            }
            if (!snapshot.hasData) {
              return SignInPage();
            }
            return MainView();
          },
        ),
        routes: <String, WidgetBuilder>{
          newGameRoute: (BuildContext context) => const NewGame(),
          gameDetailRoute: (BuildContext context) =>
              GameDetail(props: props(context)),
          chatRoute: (BuildContext context) => Chat(props: props(context)),
          profileRoute: (BuildContext context) =>
              Profile(props: props(context)),
          introductionRoute: (BuildContext context) => const Introduction(),
          settingsRoute: (BuildContext context) => const Settings(),
          aboutUs: (BuildContext context) => const AboutUs(),
          termOfServiceRoute: (BuildContext context) => const TermOfService(),
          // "#": (BuildContext context) => MainView(),
        });
  }
}
