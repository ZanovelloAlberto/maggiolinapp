// File generated by FlutterFire CLI.
// ignore_for_file: lines_longer_than_80_chars
import 'package:firebase_core/firebase_core.dart' show FirebaseOptions;
import 'package:flutter/foundation.dart'
    show defaultTargetPlatform, kIsWeb, TargetPlatform;

/// Default [FirebaseOptions] for use with your Firebase apps.
///
/// Example:
/// ```dart
/// import 'firebase_options.dart';
/// // ...
/// await Firebase.initializeApp(
///   options: DefaultFirebaseOptions.currentPlatform,
/// );
/// ```
class DefaultFirebaseOptions {
  static FirebaseOptions get currentPlatform {
    if (kIsWeb) {
      return web;
    }
    // ignore: missing_enum_constant_in_switch
    switch (defaultTargetPlatform) {
      case TargetPlatform.android:
        return android;
      case TargetPlatform.iOS:
        return ios;
      case TargetPlatform.macOS:
        throw UnsupportedError(
          'DefaultFirebaseOptions have not been configured for macos - '
          'you can reconfigure this by running the FlutterFire CLI again.',
        );
    }

    throw UnsupportedError(
      'DefaultFirebaseOptions are not supported for this platform.',
    );
  }

  static const FirebaseOptions web = FirebaseOptions(
    apiKey: 'AIzaSyATRLo1Nts5IaCRAOdxT4WKHNLB2Sd6dCM',
    appId: '1:213191270668:web:e05dabff54dbee05232929',
    messagingSenderId: '213191270668',
    projectId: 'maggiolinapp',
    authDomain: 'maggiolinapp.firebaseapp.com',
    storageBucket: 'maggiolinapp.appspot.com',
    measurementId: 'G-72FJ9HF59X',
  );

  static const FirebaseOptions android = FirebaseOptions(
    apiKey: 'AIzaSyB5pW0CNsQC0PsGiIWqqkiWRawNx8Oqnco',
    appId: '1:213191270668:android:10ee387729a955d5232929',
    messagingSenderId: '213191270668',
    projectId: 'maggiolinapp',
    storageBucket: 'maggiolinapp.appspot.com',
  );

  static const FirebaseOptions ios = FirebaseOptions(
    apiKey: 'AIzaSyAPpusLwJr53YIseWSCwoNAEl9XaNFag_g',
    appId: '1:213191270668:ios:502a52c6dda2f092232929',
    messagingSenderId: '213191270668',
    projectId: 'maggiolinapp',
    storageBucket: 'maggiolinapp.appspot.com',
    iosClientId: '213191270668-mv52k1a9r9jbdn3ehohmun010er8jkua.apps.googleusercontent.com',
    iosBundleId: 'com.maggiolina.app',
  );
}
