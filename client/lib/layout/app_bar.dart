import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:maggiolinapp/app.dart';
import 'package:maggiolinapp/model/app_model.dart';
import 'package:maggiolinapp/screens/routes/profile.dart';

AppBar MaggiolinaAppBar(BuildContext context) {
  return AppBar(
    title: Text("Maggiolina"),
    leading: IconButton(
      onPressed: () {
        Navigator.pushNamed(context, App.profileRoute,
            arguments: new ProfileProps(currentUser.uid));
      },
      icon: CircleAvatar(
        backgroundImage: NetworkImage(currentUserPhotoUrl),
      ),
    ),
    actions: [
      PopupMenuButton(
        icon: Icon(Icons.more_vert),
        itemBuilder: (BuildContext context) => <PopupMenuEntry>[
          PopupMenuItem(
            child: ListTile(
              leading: Icon(Icons.business),
              title: Text('Maggiolina'),
              onTap: () {
                Navigator.pushNamed(context, App.aboutUs);
              },
            ),
          ),
          PopupMenuItem(
            child: ListTile(
              leading: Icon(Icons.login),
              title: Text('Introduction'),
              onTap: () {
                Navigator.pushNamed(context, App.introductionRoute);
              },
            ),
          ),
          PopupMenuItem(
            child: ListTile(
              leading: Icon(Icons.person_add),
              title: Text('Invite firend'),
              onTap: () {
                Clipboard.setData(ClipboardData(text: "https://google.com"));
                ScaffoldMessenger.of(context).showSnackBar(SnackBar(
                  content: Text("Link copied in your clipboard"),
                  backgroundColor: Colors.green,
                  duration: Duration(seconds: 1),
                ));
              },
            ),
          ),
          PopupMenuItem(
            child: ListTile(
              leading: Icon(Icons.settings),
              title: Text('Settings'),
              onTap: () {
                Navigator.pushNamed(context, App.settingsRoute);
              },
            ),
          ),
          const PopupMenuDivider(),
          PopupMenuItem(
            child: ListTile(
              visualDensity: VisualDensity(horizontal: 0.5),
              leading: Icon(Icons.article),
              onTap: () {
                Navigator.pushNamed(context, App.termOfServiceRoute);
              },
              title: Text('Term of service'),
            ),
          ),
          PopupMenuItem(
            child: ListTile(
              visualDensity: VisualDensity(horizontal: 0.5),
              leading: Icon(Icons.logout),
              onTap: () => showDialog<String>(
                  context: context,
                  builder: (BuildContext context) => AlertDialog(
                        // title: const Text('Logout'),
                        content:
                            const Text('Are you sure do you want log out?'),
                        actions: <Widget>[
                          TextButton(
                            onPressed: () => Navigator.pop(context),
                            child: const Text('Cancel'),
                          ),
                          TextButton(
                            onPressed: () {
                              Navigator.pop(context);
                              FirebaseAuth.instance.signOut();
                            },
                            child: const Text('Logout'),
                          ),
                        ],
                      )),
              title: Text('Logout'),
            ),
          ),
        ],
      ),
      // IconButton(
      //     onPressed: () {
      //       Navigator.pushNamed(context, "");
      //     },
      //     icon: Icon(Icons.settings)),
      Container(
        width: 10,
      )
    ],
  );
}



// class MaggiolinaAppBar extends Widget with PreferredSizeWidget {
//   const MaggiolinaAppBar({Key? key}) : super(key: key);

//   @override
//   Widget build(BuildContext context) {
//     return AppBar(
//         title: const Text('Maggiolina'),
//         actions: [
//           IconButton(
//               onPressed: () {
//                 Navigator.pushNamed(context, "");
//               },
//               icon: Icon(Icons.settings)),
//           PopupMenuButton(
//             icon: Icon(Icons.more_vert),
//             itemBuilder: (BuildContext context) => <PopupMenuEntry>[
//               const PopupMenuItem(
//                 child: ListTile(
//                   leading: Icon(Icons.add),
//                   title: Text('Item 1'),
//                 ),
//               ),
//               const PopupMenuItem(
//                 child: ListTile(
//                   leading: Icon(Icons.anchor),
//                   title: Text('Item 2'),
//                 ),
//               ),
//               const PopupMenuItem(
//                 child: ListTile(
//                   leading: Icon(Icons.article),
//                   title: Text('Item 3'),
//                 ),
//               ),
//               const PopupMenuDivider(),
//               const PopupMenuItem(child: Text('Item A')),
//               const PopupMenuItem(child: Text('Item B')),
//             ],
//           ),
//         ],
//         leading: IconButton(
//           onPressed: () {},
//           icon: CircleAvatar(
//             backgroundImage:
//                 NetworkImage(FirebaseAuth.instance.currentUser?.photoURL ?? ""),
//           ),
//         ));
//   }

//   @override
//   Element createElement() {
//     // TODO: implement createElement
//     return AppBar().createElement();
//   }

//   @override
//   // TODO: implement preferredSize
//   Size get preferredSize => AppBar().preferredSize;
// }
