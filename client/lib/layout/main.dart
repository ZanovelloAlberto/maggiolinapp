import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter/material.dart';
import 'package:maggiolinapp/app.dart';
import 'package:maggiolinapp/layout/app_bar.dart';
import 'package:maggiolinapp/model/app_model.dart';
import 'package:maggiolinapp/screens/navbar/games.dart';
import 'package:maggiolinapp/screens/navbar/home.dart';
import 'package:provider/provider.dart';

import '../screens/navbar/messages.dart';
import '../screens/routes/chat.dart';

Widget? getFloatingButtons(BuildContext context, int index) {
  final floatingButtons = [
    null,
    FloatingActionButton(
      onPressed: () {
        Navigator.pushNamed(context, App.newGameRoute);
      },
      child: Icon(Icons.add),
    ),
    null,
    FloatingActionButton(
      onPressed: () {
        Navigator.pushNamed(context, App.chatRoute,
            arguments: ChatProps(chatId: "chatId"));
      },
      child: Icon(Icons.add_comment),
    ),
  ];
  return floatingButtons[index];
}

class MainView extends StatefulWidget {
  const MainView({Key? key}) : super(key: key);

  @override
  State<MainView> createState() => _MainViewState();
}

class _MainViewState extends State<MainView> {
  int _selectedIndex = 0;
  // static const TextStyle optionStyle =
  //     TextStyle(fontSize: 30, fontWeight: FontWeight.bold);
  static final List<Widget> _widgetOptions = <Widget>[
    const Home(),
    Games(),
    const Messages(),
  ];

  void _onItemTapped(int index) {
    setState(() {
      _selectedIndex = index;
    });
  }

  @override
  Widget build(BuildContext context) {
    return MultiProvider(
      providers: [
        ChangeNotifierProvider<AppModel>(create: (context) => AppModel()),
      ],
      child: Scaffold(
        floatingActionButton: getFloatingButtons(context, _selectedIndex),
        appBar: MaggiolinaAppBar(context),
        body: Center(
          child: _widgetOptions.elementAt(_selectedIndex),
        ),
        bottomNavigationBar: BottomNavigationBar(
          items: const <BottomNavigationBarItem>[
            BottomNavigationBarItem(
              icon: Icon(Icons.home),
              label: 'Home',
              backgroundColor: Colors.red,
            ),
            BottomNavigationBarItem(
              icon: Icon(Icons.sports_soccer),
              label: 'Games',
              backgroundColor: Colors.green,
            ),
            BottomNavigationBarItem(
              icon: Icon(Icons.message),
              label: 'Messages',
              backgroundColor: Colors.purple,
            ),
          ],
          currentIndex: _selectedIndex,
          selectedItemColor: Colors.amber[800],
          onTap: _onItemTapped,
        ),
      ),
    );
  }
}
