import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter/material.dart';

enum PrivateScreen { games, chat, newGame, profile, homePage }

class AppModel with ChangeNotifier {
  PrivateScreen screen = PrivateScreen.homePage;
  void changeScreen(PrivateScreen screen) {
    this.screen = screen;
    notifyListeners();
  }
}

User get currentUser {
  return FirebaseAuth.instance.currentUser!;
}

String get currentUserPhotoUrl {
  return FirebaseAuth.instance.currentUser?.photoURL ??
      "https://lh3.googleusercontent.com/-cXXaVVq8nMM/AAAAAAAAAAI/AAAAAAAAAKI/_Y1WfBiSnRI/photo.jpg?sz=128";
}
