import 'dart:convert';

import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:dash_chat/dash_chat.dart';

class ChatModel {
  List<ChatMessage> messages = [];
  List<String> usersId = [];

  ChatModel.fromJson(Map json) {
    messages = jsonDecode(json["messages"]);
    usersId = jsonDecode(json["usersId"]);
  }

  Map<String, Object?> toJson() {
    return {
      "messages": jsonEncode(messages),
      "usersId": jsonEncode(usersId),
    };
  }

  static final chats = FirebaseFirestore.instance
      .collection('chats')
      .withConverter<ChatModel>(
        fromFirestore: (snapshot, _) => ChatModel.fromJson(snapshot.data()!),
        toFirestore: (message, _) => message.toJson(),
      );
  Future<void> addMessage(ChatMessage message, String chatId) async {
    messages.add(message);
    await chats.doc(chatId).update({
      "messages": messages,
    });
  }
}
