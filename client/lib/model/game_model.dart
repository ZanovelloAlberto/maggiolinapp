import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:flutter/material.dart';

class Sports {
  static get values => [soccer, basket, volleyBall];
  static String soccer = "soccer";
  static String basket = "basket";
  static String volleyBall = "volleyBall";

  static get icons => {
        basket: Icons.sports_basketball,
        soccer: Icons.sports_soccer,
        volleyBall: Icons.sports_volleyball
      };
}

class GameModel {
  GameModel({
    required this.sport,
    required this.teamPlayers,
    required this.missingPlayer,
    required this.field,
    required this.title,
    required this.date,
    required this.price,
    required this.note,
  });

  String note;
  String sport;
  int teamPlayers;
  int missingPlayer;
  String field;
  String title;
  DateTime date;
  String price;

  GameModel.fromJson(Map<String, Object?> json)
      : this(
          note: json['note']! as String,
          sport: json['sport']! as String,
          teamPlayers: json['teamPlayers']! as int,
          missingPlayer: json['missingPlayer']! as int,
          field: json['field']! as String,
          title: json['title']! as String,
          date: (json['date'] as Timestamp).toDate(),
          price: json['price']! as String,
        );

  Map<String, Object?> toJson() {
    return {
      'note': note,
      'sport': sport,
      'teamPlayers': teamPlayers,
      'missingPlayer': missingPlayer,
      'field': field,
      'title': title,
      'date': date,
      'price': price,
    };
  }

  static Future<void> deleteGame(String gameId) async {
    return await games.doc(gameId).delete();
  }

  static final games = FirebaseFirestore.instance
      .collection('games')
      .withConverter<GameModel>(
        fromFirestore: (snapshot, _) => GameModel.fromJson(snapshot.data()!),
        toFirestore: (movie, _) => movie.toJson(),
      );

  static Future<DocumentSnapshot<GameModel>> getGame(String id) {
    return games.doc(id).get();
  }
}
