import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:dash_chat/dash_chat.dart';
import 'package:flutter/material.dart';
import 'package:maggiolinapp/model/app_model.dart';

class UserModel extends ChatUser {
  UserModel({
    required this.skipIntro,
  }) : super();
  bool? skipIntro;

  UserModel.fromJson(Map<dynamic, dynamic> json) : super.fromJson(json) {
    skipIntro = (json['skipIntro'] as bool);
    // final pName = json["name"] as String?;
    // uid = json['uid'];
    // name = pName ?? "$firstName $lastName";
    // firstName = json['firstName'];
    // lastName = json['lastName'];
    // avatar = json['avatar'];
    // containerColor =
    // json['containerColor'] != null ? Color(json['containerColor']) : null;
    // color = json['color'] != null ? Color(json['color']) : null;
    // customProperties = json['customProperties'] as Map<String, dynamic>?;
  }

  @override
  Map<String, Object?> toJson() {
    var result = super.toJson();
    result['skipIntro'] = skipIntro;
    return result;
  }

  static var users = FirebaseFirestore.instance
      .collection('users')
      .withConverter<UserModel>(
        fromFirestore: (snapshot, _) => UserModel.fromJson(snapshot.data()!),
        toFirestore: (message, _) => message.toJson(),
      );

  static DocumentReference<UserModel> user(String userId) {
    return users.doc(userId);
  }

  static get current {
    return users.doc(currentUser.uid);
  }
}
