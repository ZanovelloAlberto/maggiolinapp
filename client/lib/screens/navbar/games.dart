import 'package:badges/badges.dart';
import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:dash_chat/dash_chat.dart';
import 'package:flutter/material.dart';
import 'package:maggiolinapp/app.dart';
import 'package:maggiolinapp/model/game_model.dart';
import 'package:maggiolinapp/screens/routes/game.dart';

class Games extends StatelessWidget {
  Games({Key? key}) : super(key: key);

  // final List<GameModel> games = [
  //   GameModel(
  //     id: "string",
  //     note: "string",
  //     sport: Sports.basket,
  //     teamPlayers: 3,
  //     missingPlayer: 1,
  //     field: "bocampo",
  //     title: "parita bellla",
  //     price: "\$ 10",
  //     date: DateTime.tryParse("10/03/2020") ?? DateTime.now(),
  //   ),
  //   GameModel(
  //     id: "34",
  //     note: "string",
  //     sport: Sports.basket,
  //     teamPlayers: 5,
  //     missingPlayer: 0,
  //     field: "bio campo",
  //     title: "baloncesto",
  //     price: "\$ 10",
  //     date: DateTime.tryParse("10/03/2020") ?? DateTime.now(),
  //   ),
  //   GameModel(
  //     id: "strsdfing",
  //     note: "string",
  //     sport: Sports.soccer,
  //     teamPlayers: 5,
  //     missingPlayer: 3,
  //     field: "numbero 8 in altro a distro",
  //     title: "uno che",
  //     price: "\$ 10",
  //     date: DateTime.tryParse("10/03/2020") ?? DateTime.now(),
  //   ),
  //   GameModel(
  //     id: "strsdfing",
  //     note: "string",
  //     sport: Sports.volleyBall,
  //     teamPlayers: 5,
  //     missingPlayer: 3,
  //     field: "bocampo",
  //     title: "youris megoli",
  //     price: "\$ 10",
  //     date: DateTime.tryParse("10/03/2020") ?? DateTime.now(),
  //   ),
  // ];

  @override
  Widget build(BuildContext context) {
    return StreamBuilder<QuerySnapshot<GameModel>>(
        stream: GameModel.games.snapshots(),
        builder: (context, snapshot) {
          if (snapshot.data == null) {
            return const Center(
              child: CircularProgressIndicator(),
            );
          }

          return Column(
            children: [
              Padding(
                padding: const EdgeInsets.all(4.0),
                child: AppBar(
                  toolbarHeight: 40,
                  backgroundColor: Colors.grey,
                  actions: [
                    IconButton(
                      onPressed: () {},
                      icon: Icon(Icons.date_range),
                    ),
                    IconButton(
                      onPressed: () {},
                      icon: Icon(Icons.list_outlined),
                    ),
                  ],
                ),
              ),
              if (snapshot.data!.docs.length == 0)
                Expanded(
                  child: Text("no elements found"),
                ),
              Expanded(
                // height: 400,
                child: ListView.builder(
                    itemCount: snapshot.data?.docs.length ?? 0,
                    itemBuilder: (context, i) {
                      var game = snapshot.data!.docs[i].data();
                      return ListTile(
                        isThreeLine: true,
                        subtitle: Text(game.title),
                        leading: Icon(Sports.icons[game.sport]),
                        onTap: () {
                          Navigator.pushNamed(context, App.gameDetailRoute,
                              arguments: GameProps(snapshot.data!.docs[i].id));
                        },
                        trailing: FittedBox(
                          child: Column(
                            children: [
                              Text(DateFormat('kk:mm').format(game.date)),
                              if (game.missingPlayer > 0)
                                Badge(
                                    badgeContent:
                                        Text(game.missingPlayer.toString()))
                              else
                                Badge(
                                    badgeColor: Colors.green,
                                    badgeContent: Icon(
                                      Icons.check,
                                      size: 8,
                                    )),
                              Text(game.price),
                            ],
                          ),
                        ),
                        title: Text(game.title),
                      );
                    }),
              ),
            ],
          );
        });
  }

  // Widget buildItemListGame(BuildContext context, int index) {
  //   // GameModel game = games[index];
  //   return ListTile(
  //     isThreeLine: true,
  //     subtitle: Text(game.title),
  //     leading: Icon(Sports.icons[game.sport]),
  //     onTap: () {
  //       Navigator.pushNamed(context, App.gameDetailRoute,
  //           arguments: GameProps(snapshot.data!.docs[i] ?? ""));
  //     },
  //     trailing: FittedBox(
  //       child: Column(
  //         children: [
  //           Text(DateFormat('kk:mm').format(game.date)),
  //           if (game.missingPlayer > 0)
  //             Badge(badgeContent: Text(game.missingPlayer.toString()))
  //           else
  //             Badge(
  //                 badgeColor: Colors.green,
  //                 badgeContent: Icon(
  //                   Icons.check,
  //                   size: 8,
  //                 )),
  //           Text(game.price),
  //         ],
  //       ),
  //     ),
  //     title: Text(game.title),
  //   );
  // }
}
