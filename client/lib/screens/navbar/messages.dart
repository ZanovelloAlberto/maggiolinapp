import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter/material.dart';
import 'package:maggiolinapp/model/app_model.dart';
import 'package:maggiolinapp/model/user_model.dart';
import 'package:provider/provider.dart';

class Messages extends StatelessWidget {
  const Messages({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    var list = FirebaseFirestore.instance
        .collection("users")
        .withConverter<UserModel>(
          fromFirestore: (snapshot, _) => UserModel.fromJson(snapshot.data()!),
          toFirestore: (user, _) => user.toJson(),
        )
        .snapshots();

    // if ()

    return Scaffold(
      body: Center(
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          children: [
            const Text("HOME"),
            TextButton(
              onPressed: () {
                FirebaseAuth.instance.signOut();
              },
              child: const Text("Sign out"),
            ),
            TextButton(
              onPressed: () {
                Provider.of<AppModel>(context, listen: false)
                    .changeScreen(PrivateScreen.chat);
              },
              child: Text("sdlf"),
            ),
          ],
        ),
      ),
    );
  }
}
