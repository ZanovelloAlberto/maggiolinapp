import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:dash_chat/dash_chat.dart';
import 'package:flutter/material.dart';
import 'package:maggiolinapp/model/app_model.dart';
import 'package:maggiolinapp/model/chat_model.dart';

class ChatProps {
  String chatId;
  ChatProps({required this.chatId});
}

class Chat extends StatefulWidget {
  const Chat({Key? key, required this.props}) : super(key: key);
  final ChatProps props;
  @override
  State<Chat> createState() => _ChatState();
}

class _ChatState extends State<Chat> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        leading: IconButton(
          icon: Icon(Icons.arrow_back),
          onPressed: () => Navigator.pop(context),
        ),
        title: Text("Simio"),
      ),
      body: StreamBuilder<DocumentSnapshot<ChatModel>>(
          stream: ChatModel.chats.doc(widget.props.chatId).snapshots(),
          builder: (context, snapshot) {
            var chat = snapshot.data;

            if (snapshot.connectionState == ConnectionState.waiting ||
                chat == null) {
              return const Center(
                child: CircularProgressIndicator(),
              );
            }
            return DashChat(
                dateFormat: DateFormat("hh-mm"),
                onSend: (v) {
                  chat.data()?.addMessage(v, widget.props.chatId);
                },
                user: ChatUser(
                  avatar: currentUserPhotoUrl,
                  firstName: currentUser.displayName,
                  name: currentUser.displayName,
                  uid: currentUser.uid,
                ),
                messages: chat.data()!.messages);
          }),
    );
  }
}
