// import 'package:badges/badges.dart';
import 'package:dash_chat/dash_chat.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:maggiolinapp/model/game_model.dart';
import 'package:maggiolinapp/widget/settings/time_picker.dart';
import 'package:settings_ui/settings_ui.dart';
// import 'package:syncfusion_flutter_datepicker/datepicker.dart';

class EditGameProps {
  String gameId;
  EditGameProps(this.gameId);
}

class EditGame extends StatefulWidget {
  const EditGame({Key? key, required this.props}) : super(key: key);
  final EditGameProps props;
  @override
  State<EditGame> createState() => _EditGameState();
}

class _EditGameState extends State<EditGame> {
  String? _id;
  GameModel _gameModel = GameModel(
    note: "",
    sport: Sports.basket,
    teamPlayers: 1,
    missingPlayer: 1,
    field: "",
    title: "",
    date: DateTime.now(),
    price: "",
  );

  @override
  void initState() async {
    final args = ModalRoute.of(context)?.settings.arguments as EditGameProps;
    _gameModel = (await GameModel.getGame(args.gameId)).data()!;
    _id = args.gameId;
    super.initState();
  }

  Widget _body(BuildContext context) {
    if (_id == null) {
      return const Center(
        child: CircularProgressIndicator(),
      );
    }

    return SettingsList(
      sections: [
        SettingsSection(
          tiles: [
            SettingsTile(
              // title: const Text("Name"),
              title: TextField(
                decoration: const InputDecoration(
                  border: UnderlineInputBorder(),
                  label: Text("Name"),
                ),
                onChanged: (v) {
                  _gameModel.title = v;
                },
              ),
            )
          ],
        ),
        SettingsSection(
          // title: Text("Sport"),
          tiles: [
            SettingsTile(
              title: Text("Sport"),
              description: DropdownButton(
                isExpanded: true,
                value: _gameModel.sport,
                items:
                    Sports.values.map<DropdownMenuItem<String>>((String value) {
                  return DropdownMenuItem<String>(
                    value: value,
                    child: Text(value),
                  );
                }).toList(),
                onChanged: (String? value) {
                  setState(() {
                    _gameModel.sport = value ?? Sports.soccer;
                  });
                },
              ),
            ),
            SettingsTile(
              title: Text("player number"),
              description: DropdownButton(
                isExpanded: true,
                value: "5 vs 5",
                items: ["5 vs 5", "2 vs 2"]
                    .map<DropdownMenuItem<String>>((String value) {
                  return DropdownMenuItem<String>(
                    value: value,
                    child: Text(value),
                  );
                }).toList(),
                onChanged: (String? value) {
                  // setState(() {
                  //   _gameModel.sport = value ?? Sports.soccer;
                  // });
                },
              ),
            )
          ],
        ),
        SettingsSection(tiles: [
          SettingsTile(
            title: Text("Date"),
            description: Text(DateFormat("yyyy-MM-dd").format(_gameModel.date)),
            onPressed: (context) async {
              var a = await myShowDatePicker(
                  context: context, date: _gameModel.date);
              if (a != null) {
                setState(() {
                  _gameModel.date = DateTime(a.year, a.month, a.day);
                });
              }
            },
          ),
          SettingsTile(
            title: Text("Time"),
            description: Text(
                DateFormat(DateFormat.HOUR24_MINUTE).format(_gameModel.date)),
            onPressed: (context) async {
              var a = await showTimePicker(
                  context: context,
                  initialTime: TimeOfDay.fromDateTime(_gameModel.date));
              if (a != null) {
                setState(() {
                  // _gameModel.date.
                });
              }
            },
          )
        ]),
        SettingsSection(
          tiles: [
            SettingsTile(
              title: Text("Payment"),
              description: TextField(
                keyboardType: TextInputType.number,
                inputFormatters: <TextInputFormatter>[
                  FilteringTextInputFormatter.digitsOnly
                ],
              ),
            ),
          ],
        ),
        SettingsSection(
          tiles: [
            SettingsTile(
                title: Text("Note"),
                description: TextField(
                  onChanged: (v) {
                    setState(() {
                      _gameModel.note = v;
                    });
                  },
                )),
            SettingsTile(
                title: TextButton(
                    onPressed: () {
                      if (_id == null) {
                        GameModel.games.add(_gameModel);
                      }
                    },
                    child: Text("Confirm")))
          ],
        )
      ],
    );
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        leading: IconButton(
          icon: Icon(Icons.arrow_back),
          onPressed: () => Navigator.pop(context),
        ),
        title: Text("Create Game"),
      ),
      body: _body(context),
    );
  }
}
