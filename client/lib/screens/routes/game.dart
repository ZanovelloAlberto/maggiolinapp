import 'package:flutter/material.dart';
import 'package:maggiolinapp/model/game_model.dart';

class GameProps {
  String gameId;
  GameProps(this.gameId);
}

class GameDetail extends StatelessWidget {
  const GameDetail({Key? key, required this.props}) : super(key: key);
  final GameProps props;
  @override
  Widget build(BuildContext context) {
    final args = ModalRoute.of(context)?.settings.arguments as GameProps;

    return Scaffold(
      appBar: AppBar(
        leading: IconButton(
          icon: Icon(Icons.arrow_back),
          onPressed: () {
            Navigator.pop(context);
          },
        ),
        title: Text("Create Game"),
        actions: [
          IconButton(
            onPressed: () {
              Navigator.pop(context);
              GameModel.deleteGame(props.gameId).then((value) =>
                  ScaffoldMessenger.of(context).showSnackBar(SnackBar(
                    content: Text("Deleted"),
                    backgroundColor: Colors.green,
                    duration: Duration(seconds: 1),
                  )));
            },
            icon: const Icon(Icons.delete),
          )
        ],
      ),
      body: Column(
        children: [
          Text("New Game"),
          TextButton(
            onPressed: () {},
            child: Text(args.gameId),
          )
        ],
      ),
    );
  }
}
