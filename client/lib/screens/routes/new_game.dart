// import 'package:badges/badges.dart';
import 'dart:html';

import 'package:dash_chat/dash_chat.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:intl/date_time_patterns.dart';
import 'package:maggiolinapp/model/game_model.dart';
import 'package:maggiolinapp/widget/dialog/date_dialog.dart';
import 'package:maggiolinapp/widget/settings/time_picker.dart';
import 'package:settings_ui/settings_ui.dart';
// import 'package:syncfusion_flutter_datepicker/datepicker.dart';

class NewGame extends StatefulWidget {
  const NewGame({Key? key}) : super(key: key);

  @override
  State<NewGame> createState() => _NewGameState();
}

class _NewGameState extends State<NewGame> {
  GameModel _gameModel = GameModel(
    note: "",
    sport: Sports.basket,
    teamPlayers: 1,
    missingPlayer: 1,
    field: "",
    title: "",
    date: DateTime.now(),
    price: "",
  );
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        leading: IconButton(
          icon: Icon(Icons.arrow_back),
          onPressed: () => Navigator.pop(context),
        ),
        title: Text("Create Game"),
      ),
      body: SettingsList(
        sections: [
          SettingsSection(
            tiles: [
              SettingsTile(
                // title: const Text("Name"),
                title: TextField(
                  decoration: const InputDecoration(
                    border: UnderlineInputBorder(),
                    label: Text("Name"),
                  ),
                  onChanged: (v) {
                    _gameModel.title = v;
                  },
                ),
              )
            ],
          ),
          SettingsSection(
            // title: Text("Sport"),
            tiles: [
              SettingsTile(
                title: Text("Sport"),
                description: DropdownButton(
                  isExpanded: true,
                  value: _gameModel.sport,
                  items: Sports.values
                      .map<DropdownMenuItem<String>>((String value) {
                    return DropdownMenuItem<String>(
                      value: value,
                      child: Text(value),
                    );
                  }).toList(),
                  onChanged: (String? value) {
                    setState(() {
                      _gameModel.sport = value ?? Sports.soccer;
                    });
                  },
                ),
              ),
              SettingsTile(
                title: Text("player number"),
                description: DropdownButton(
                  isExpanded: true,
                  value: "5 vs 5",
                  items: ["5 vs 5", "2 vs 2"]
                      .map<DropdownMenuItem<String>>((String value) {
                    return DropdownMenuItem<String>(
                      value: value,
                      child: Text(value),
                    );
                  }).toList(),
                  onChanged: (String? value) {
                    // setState(() {
                    //   _gameModel.sport = value ?? Sports.soccer;
                    // });
                  },
                ),
              )
            ],
          ),
          SettingsSection(tiles: [
            SettingsTile(
              title: Text("Date"),
              description:
                  Text(DateFormat("yyyy-MM-dd").format(_gameModel.date)),
              onPressed: (context) async {
                var a = await myShowDatePicker(context: context);
                if (a != null) {
                  setState(() {
                    _gameModel.date = DateTime(a.year, a.month, a.day,
                        _gameModel.date.hour, _gameModel.date.minute);
                  });
                }
              },
            ),
            SettingsTile(
              title: Text("Time"),
              description: Text(
                  DateFormat(DateFormat.HOUR24_MINUTE).format(_gameModel.date)),
              onPressed: (context) async {
                var a = await showTimePicker(
                    context: context,
                    initialTime: TimeOfDay.fromDateTime(_gameModel.date));
                if (a != null) {
                  setState(() {
                    _gameModel.date = DateTime(
                      _gameModel.date.year,
                      _gameModel.date.month,
                      _gameModel.date.day,
                      a.hour,
                      a.minute,
                    );
                  });
                }
              },
            )
          ]),
          SettingsSection(
            tiles: [
              SettingsTile(
                // title: Text("Payment"),
                title: TextField(
                  decoration: const InputDecoration(label: Text("Payment")),
                  keyboardType:
                      const TextInputType.numberWithOptions(decimal: true),
                  inputFormatters: <TextInputFormatter>[
                    FilteringTextInputFormatter.digitsOnly
                  ],
                ),
              ),
            ],
          ),
          SettingsSection(
            tiles: [
              SettingsTile(
                  // title: Text("Note"),
                  title: TextField(
                decoration: InputDecoration(label: Text("Note")),
                onChanged: (v) {
                  setState(() {
                    _gameModel.note = v;
                  });
                },
              )),
            ],
          ),
          SettingsSection(tiles: [
            SettingsTile(
              title: Align(
                alignment: Alignment.bottomRight,
                child: TextButton(
                  onPressed: () {
                    GameModel.games.add(_gameModel);
                    Navigator.pop(context);
                  },
                  child: Text("Confirm"),
                ),
              ),
            )
          ])
        ],
      ),
    );
  }
}
