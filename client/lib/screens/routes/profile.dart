import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:flutter/material.dart';
import 'package:maggiolinapp/model/app_model.dart';
import 'package:maggiolinapp/model/user_model.dart';
import 'package:maggiolinapp/widget/profile_image.dart';

class ProfileProps {
  String userId;
  ProfileProps(this.userId);
}

class Profile extends StatefulWidget {
  const Profile({required this.props});
  final ProfileProps props;
  @override
  State<Profile> createState() => _ProfileState();
}

class _ProfileState extends State<Profile> {
  @override
  Widget build(BuildContext context) {
    return StreamBuilder<DocumentSnapshot<UserModel>>(
        stream: UserModel.user(widget.props.userId).snapshots(),
        builder: (context, snapshot) {
          return Scaffold(
            appBar: AppBar(
              backgroundColor: Colors.transparent,
              elevation: 0,
              leading: IconButton(
                icon: Icon(
                  Icons.arrow_back,
                  color: Colors.black,
                ),
                onPressed: () => Navigator.pop(context),
              ),
              // title: Text(
              //   "daf",
              //   style: TextStyle(color: Colors.black),
              // ),
              actions: [
                IconButton(
                  icon: Icon(
                    Icons.brightness_1,
                    color: Colors.black,
                  ),
                  onPressed: () => Navigator.pop(context),
                ),
              ],
            ),
            body: ListView(
              children: [
                ProfileImage(imagePath: currentUserPhotoUrl),
              ],
            ),
          );
        });
  }
}
