import 'package:badges/badges.dart';
import 'package:flutter/material.dart';
import 'package:maggiolinapp/screens/introduction.dart';
import 'package:settings_ui/settings_ui.dart';

class Settings extends StatefulWidget {
  const Settings({Key? key}) : super(key: key);

  @override
  State<Settings> createState() => _SettingsState();
}

class _SettingsState extends State<Settings> {
  String? cvalue;
  String? titleDialog;
  bool? ena;
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        leading: IconButton(
          icon: Icon(Icons.arrow_back),
          onPressed: () => Navigator.pop(context),
        ),
        title: Text("Settings"),
      ),
      body: SettingsList(
        sections: [
          SettingsSection(
            title: Text('Common'),
            tiles: <SettingsTile>[
              SettingsTile.navigation(
                leading: Icon(Icons.language),
                title: Text('sdfsdf'),
                value: Text('Englisdfsdfsh'),
                description: Text("descritpion"),
                trailing: Text("trailling"),
                onPressed: (context) {
                  showDialog<String>(
                    context: context,
                    builder: (BuildContext context) => AlertDialog(
                      title: const Text('AlertDialog Title'),
                      // content: const Text('AlertDialog description'),
                      actions: <Widget>[
                        TextButton(
                          onPressed: () => Navigator.pop(context, 'Cancel'),
                          child: const Text('Cancel'),
                        ),
                        TextButton(
                          onPressed: () => Navigator.pop(context, 'OK'),
                          child: const Text('OK'),
                        ),
                      ],
                    ),
                  );
                },
              ),
              SettingsTile(
                title: Text("Sport"),
                value: Text(titleDialog ?? "insert"),
                description: Text("insert cose"),
                onPressed: (context) {
                  var a = showDialog<String>(
                    context: context,
                    builder: (BuildContext context) => SimpleDialog(
                      title: Center(
                          child: Badge(
                        badgeColor: Colors.green,
                        badgeContent: Text("0"),
                      )),

                      children: [
                        TextButton(
                          child: Text("res"),
                          onPressed: () {
                            Navigator.pop(context, "res");
                          },
                        ),
                        TextButton(
                          child: Text("ugg"),
                          onPressed: () {
                            Navigator.pop(context, "ugg");
                          },
                        ),
                      ],

                      // content: const Text('AlertDialog description'),
                    ),
                  );
                  a.then((v) => setState(() {
                        titleDialog = v;
                      }));
                },
              ),
              SettingsTile(
                title: Text("intro"),
                onPressed: (context) {
                  showDialog(
                      context: context,
                      builder: (c) {
                        return (Dialog(
                          child: Container(
                            child: Introduction(),
                            width: 400,
                            height: 400,
                          ),
                        ));
                      });
                },
              ),
              SettingsTile(
                leading: Icon(Icons.language),
                title: Text("Lenguage"),
                onPressed: (context) {},
                description: DropdownButton(
                  isExpanded: true,
                  value: cvalue ?? "One",
                  items: <String>['One', 'Two', 'Free', 'Four']
                      .map<DropdownMenuItem<String>>((String value) {
                    return DropdownMenuItem<String>(
                      value: value,
                      child: Text(value),
                    );
                  }).toList(),
                  onChanged: (String? value) {
                    setState(() {
                      cvalue = value;
                    });
                  },
                ),
              ),
              SettingsTile.switchTile(
                onToggle: (value) {
                  setState(() {
                    ena = value;
                  });
                },
                initialValue: ena,
                // enabled: ena ?? true,
                leading: Icon(Icons.format_paint),
                title: Text('Enable custom theme'),
              ),
            ],
          ),
        ],
      ),
    );
  }
}
