// SvgPicture.asset

import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_markdown/flutter_markdown.dart';

class TermOfService extends StatelessWidget {
  const TermOfService({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: const Text("Term of service"),
        leading: IconButton(
          icon: const Icon(Icons.arrow_back),
          onPressed: () => Navigator.pop(context),
        ),
      ),
      body: Scrollbar(
        isAlwaysShown: true,
        showTrackOnHover: true,
        child: SingleChildScrollView(
          padding: const EdgeInsets.all(40),
          child: Center(
            child: Column(
              children: [
                Image.asset("images/service_terms.png"),
                StreamBuilder<String>(
                  stream:
                      rootBundle.loadString("md/service_terms.md").asStream(),
                  builder: ((context, snapshot) {
                    if (snapshot.hasData && snapshot.data != null) {
                      return MarkdownBody(
                        data: snapshot.data ?? "",
                      );
                      // print(snapshot.data);
                    }
                    return CircularProgressIndicator();
                  }),
                )
              ],
            ),
          ),
        ),
      ),
    );
  }
}
