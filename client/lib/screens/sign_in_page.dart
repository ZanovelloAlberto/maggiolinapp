// ignore: file_names
import 'package:flutter/material.dart';
import 'package:flutterfire_ui/auth.dart';

// ignore: use_key_in_widget_constructors
class SignInPage extends StatelessWidget {
  final TextEditingController emailController = TextEditingController();
  final TextEditingController passwordController = TextEditingController();

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Center(
        child: Container(
          width: 350,
          padding: const EdgeInsets.only(top: 100),
          child: Column(
            children: [
              const Padding(
                padding: EdgeInsets.only(bottom: 30.0),
                child: Text(
                  "Sign in",
                  style: TextStyle(fontSize: 50),
                ),
              ),
              Container(
                child: EmailForm(),
              ),
              const Padding(
                  padding: EdgeInsets.only(top: 30, bottom: 30),
                  child: Divider()),
              // TextButton(
              //   onPressed: () {
              //     context.read<AuthenticationService>().signInWithGoogle();
              //   },
              //   child: const Text("Google"),
              // ),

              GoogleSignInButton(
                clientId: key.toString(),
              ),
              const AppleSignInButton()
            ],
          ),
        ),
      ),
    );
  }
}
