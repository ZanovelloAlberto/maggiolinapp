import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:dash_chat/dash_chat.dart';
import 'package:maggiolinapp/model/game_model.dart';
import 'package:maggiolinapp/model/chat_model.dart';

class FirestoreService {
  static final messages = FirebaseFirestore.instance
      .collection('messages')
      .withConverter<ChatMessage>(
        fromFirestore: (snapshot, _) => ChatMessage.fromJson(snapshot.data()!),
        toFirestore: (message, _) => message.toJson(),
      );

  static final games = FirebaseFirestore.instance
      .collection('games')
      .withConverter<GameModel>(
        fromFirestore: (snapshot, _) => GameModel.fromJson(snapshot.data()!),
        toFirestore: (movie, _) => movie.toJson(),
      );

  static Future<QuerySnapshot<GameModel>> getGames(int startAt) {
    return games
        // .where()
        // .startAt([0])
        // .limit(10)
        .get();
    // .orderBy("startDate", descending: true)
    // .snapshots();
  }

  static Future<void> createGame(GameModel game) async {
    await games.add(game);
  }

  // static Future<void> addMessage(ChatModel message) async {
  //   await messages.add(message);
  // }
}
