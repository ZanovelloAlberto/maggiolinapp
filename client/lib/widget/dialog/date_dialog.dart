import 'package:flutter/material.dart';
import 'package:maggiolinapp/widget/dialog/dialog_enum.dart';
import 'package:syncfusion_flutter_datepicker/datepicker.dart';

class DateDialog extends StatelessWidget {
  const DateDialog({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Dialog(
      // title: Center(
      //   child: Badge(
      //     badgeColor: Colors.green,
      //     badgeContent: Text("0"),
      //   ),
      // ),

      child: SizedBox(
        child: Column(
          mainAxisSize: MainAxisSize.min,
          children: [
            // Text("title"),
            SfDateRangePicker(
              initialDisplayDate: DateTime.utc(2022, 4, 22, 0, 0, 0, 0, 0),
              initialSelectedRanges: [
                PickerDateRange(
                  DateTime.now(),
                  DateTime.utc(2022, 4, 22, 0, 0, 0, 0, 0),
                )
                // PickerDateRange(
                //   startDate:
                // ),
              ],
            ),
            Row(
              mainAxisAlignment: MainAxisAlignment.end,
              children: [
                TextButton(
                    onPressed: () =>
                        Navigator.pop(context, "ConfirmDialogResults.cancel"),
                    child: Text("cancel")),
                TextButton(
                    onPressed: () =>
                        Navigator.pop(context, "ConfirmDialogResults.confirm"),
                    child: Text("confirm")),
              ],
            ),
          ],
        ),
      ),
    );
  }
}
