import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

Widget buildImage(String assetName, [double width = 350]) {
  return Image.asset('assets/images/$assetName', width: width);
}

Widget buildFullscreenImage() {
  return Image.asset(
    'assets/images/fullscreen.jpg',
    fit: BoxFit.cover,
    height: double.infinity,
    width: double.infinity,
    alignment: Alignment.center,
  );
}
