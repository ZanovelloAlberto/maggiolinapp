import 'dart:ui';

import 'package:flutter/material.dart';

class ProfileImage extends StatelessWidget {
  final String imagePath;

  const ProfileImage({Key? key, required this.imagePath}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    final image = NetworkImage(imagePath);

    return Center(
      child: Stack(
        children: [
          ClipOval(
            child: Material(
              color: Colors.transparent,
              child: Ink.image(
                image: image,
                height: 128,
                width: 128,
                child: InkWell(
                  onTap: () {},
                ),
              ),
            ),
          ),
          Positioned(
            child: builtInEditbutton(),
            bottom: 0,
            right: 4,
          ),
        ],
      ),
    );
  }

  Widget builtInEditbutton() {
    return ClipOval(
      child: Container(
        padding: EdgeInsets.all(3),
        color: Colors.white,
        child: ClipOval(
          child: Container(
            padding: const EdgeInsets.all(7),
            color: Colors.blue,
            child: const Icon(
              Icons.edit,
              size: 20,
              color: Colors.white,
            ),
          ),
        ),
      ),
    );
  }
}
