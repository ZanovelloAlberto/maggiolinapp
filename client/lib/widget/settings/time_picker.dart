import 'package:flutter/material.dart';
import 'package:settings_ui/settings_ui.dart';

// SettingsTile TimePicker(BuildContext context, String title, TimeOfDay? time) {
//   return SettingsTile(
//     title: Text(title),
//     onPressed: (context) {
//       var a = showTimePicker(
//           context: context, initialTime: time ?? TimeOfDay.now());

//       // if (a) {
//       //   setState(() {
//       //     adf = sdfkaakl;
//       //   });
//       // }
//     },
//   );
// }

Future<DateTime?> myShowDatePicker(
    {required BuildContext context, DateTime? date}) async {
  return await showDatePicker(
    context: context,
    initialDate: date ?? DateTime.now().add(const Duration(days: 3)),
    firstDate: DateTime.now(),
    lastDate: DateTime.now().add(const Duration(days: 60)),
  );
}
